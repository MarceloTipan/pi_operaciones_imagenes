#Realiza operaciones aritméticas entre imágenes a partir de sus matrices

import cv2 as cv

# Usar OpenCV para Adición de imágenes
# cv.add
# Usar OpenCV para Mezcla de imágenes
# cv.addWeughted
# Usar OpenCV para Sustracción  de imágenes
# cv.substract
# cv.absdiff

# Leer imagen Simón Bolívar
img1 = cv.imread('./mitad_mundo.jpg')
# Leer imagen Torre Eiffel
img2 = cv.imread('./torre_eiffel.jpg')

#Indica el tamaño de la imagen ancho/altura
print(img1.shape)
print(img2.shape)

#Redefine el tamaño de la imagen, para que poder realizar operaciones de matrices
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)

suma = cv.add(imagen1_res,img2)

# Realizar operaciones solicitadas
cv.imshow('MitadMundo' ,img1)
cv.imshow('TorreEiffel',img2)
cv.imshow('SumaImágenes',suma)

cv.waitKey(0)
cv.destroyAllWindows()