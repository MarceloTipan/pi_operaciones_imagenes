#Realiza operaciones lógicas entre imágenes a partir de sus matrices

import cv2 as cv

# Usar OpenCV para Adición de imágenes
# cv.add
# Usar OpenCV para Mezcla de imágenes
# cv.addWeughted
# Usar OpenCV para Sustracción  de imágenes
# cv.substract
# cv.absdiff

# Leer imagen Simón Bolívar
img1 = cv.imread('./torre_eiffel.jpg')
# Leer imagen Torre Eiffel
img2 = cv.imread('./elipse.png')

#Indica el tamaño de la imagen ancho/altura
print(img1.shape)
print(img2.shape)

#Redefine el tamaño de la imagen, para que poder realizar operaciones de matrices
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(img2, dsize=(512, 512), interpolation=cv.INTER_CUBIC)

#res = cv.bitwise_and(imagen1_res,imagen2_res)
#res = cv.bitwise_or(imagen1_res,imagen2_res)
#res = cv.bitwise_not(imagen1_res,imagen2_res)
res = cv.bitwise_xor(imagen1_res,imagen2_res)

# Realizar operaciones solicitadas
cv.imshow('cuadrado' ,imagen1_res)
cv.imshow('elipse',imagen2_res)
cv.imshow('SumaImágenes',res)

cv.waitKey(0)
cv.destroyAllWindows()