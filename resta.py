#Realiza operaciones aritméticas entre imágenes a partir de sus matrices

import cv2 as cv

# Usar OpenCV para Adición de imágenes
# cv.add
# Usar OpenCV para Mezcla de imágenes
# cv.addWeughted
# Usar OpenCV para Sustracción  de imágenes
# cv.substract
# cv.absdiff

# Leer imagen Simón Bolívar
img1 = cv.imread('./cuadrado.png')
# Leer imagen Torre Eiffel
img2 = cv.imread('./elipse.png')

#Indica el tamaño de la imagen ancho/altura
print(img1.shape)
print(img2.shape)

#Redefine el tamaño de la imagen, para que poder realizar operaciones de matrices
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(img2, dsize=(512, 512), interpolation=cv.INTER_CUBIC)

#suma = cv.add(imagen1_res,img2)
#suma = cv.addWeighted(imagen1_res,0.2,img2,0.8,0)
resta = cv.subtract(imagen1_res, imagen2_res)
#abs = cv.absdiff(imagen1_res, imagen2_res)

# Realizar operaciones solicitadas
cv.imshow('MitadMundo' ,imagen1_res)
cv.imshow('TorreEiffel',imagen2_res)
cv.imshow('SumaImágenes',resta)

cv.waitKey(0)
cv.destroyAllWindows()